/* -----------------------------------------------------------------------------
 * Example .ino file for arduino, compiled with CmdMessenger.h and
 * CmdMessenger.cpp in the sketch directory.
 *----------------------------------------------------------------------------*/

#include "CmdMessenger.h"
#include <LiquidCrystal.h>

const char *shared_key = "73bd5b184654c6aecafb6d256ddc032a";



/* Define available CmdMessenger commands */
enum
{
    server_sec_key,
    arduino_sec_key,
    button_pressed,
    error,
    error_ack
};

/* Initialize CmdMessenger -- this should match PyCmdMessenger instance */
CmdMessenger c = CmdMessenger(Serial, ',',';','/');


// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);


// Define constants and variables
const int inputPin = A0;  // buttons array analog input

uint16_t inputValue = 0;   // value read from buttons array

bool isError = false;

int hash_server_key(char *serverkey){
  long serverlong = (long)strtol(serverkey, NULL, 16);
  long keylong = (long)strtol(secretkey, NULL, 16);
  long hash = serverlong ^ keylong;
  char buf[256] = {};
  return serverlong ^ keylong;
}


/* Create callback functions to deal with incoming messages */

void on_server_sec_key(void){
    char serverkey = c.readStringArg();
    int hash = hash_server_key(serverkey);
    c.sendCmd(arduino_sec_key,hash);
}

void on_error(void){
    isError = true;
    String error = c.readStringArg();
    display_error(error);
    c.sendCmd(error_ack,error);
}

void on_button_pressed(uint16_t btn){
    c.sendBinCmd(button_pressed,btn);
}

/* Attach callbacks for CmdMessenger commands */
void attach_callbacks(){
  c.attach(server_sec_key,on_server_sec_key);
  c.attach(error, on_error);
}

void display_error(String err){
  lcd.begin(16, 2);              // set up the LCD's number of columns and rows
  lcd.print("   ERROR");  // Print a message on the LCD
  lcd.setCursor(0, 1);           // set the cursor to column 0, line 1
  lcd.print(err);

}

void display_info(){
  lcd.begin(16, 2);              // set up the LCD's number of columns and rows
  lcd.print("  GIT COMMANDER");  // Print a message on the LCD
  lcd.setCursor(0, 1);           // set the cursor to column 0, line 1
  lcd.print("PRESS ANY BUTTON");
}

void check_buttons(){
  // check buttons status
  inputValue = analogRead(inputPin);
  if(inputValue < 100 && inputValue >= 0) inputValue = 1;
  else if(inputValue < 250 && inputValue > 150) inputValue = 2;
  else if(inputValue < 470 && inputValue > 370) inputValue = 3;
  else if(inputValue < 670 && inputValue > 570) inputValue = 4;
  else if(inputValue < 870 && inputValue > 770) inputValue = 5;
  else if(inputValue <= 1023 && inputValue > 950) inputValue = 0;

  // update display text
  if(inputValue == 1 || inputValue == 2 || inputValue == 3 || inputValue == 4 || inputValue == 5)
  {
    isError = false;
    lcd.setCursor(0, 1);
    lcd.print("Pressed button:");
    lcd.print(inputValue);
    on_button_pressed(inputValue);
    delay(200);
  }
  else
  {
    if(!isError){
      lcd.setCursor(0, 1);
      lcd.print("PRESS ANY BUTTON");
    }
  }
}

void setup() {
  Serial.begin(115200);
    attach_callbacks();
    display_info();
}

void loop() {
    c.feedinSerialData();
    check_buttons();
}

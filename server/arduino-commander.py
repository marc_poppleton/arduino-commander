#!/usr/bin/env python3
__description__ = \
"""
Connects to the Arduino via Serial/USB and waits for button events.
Each button is mapped to a shell script to execute.
"""
__author__ = "Marc Poppleton"
__date__ = "2017-05-04"
__usage__ = "./arduino-commander.py config_file (see config.ini)"

import random, sys
import subprocess
import configparser
import PyCmdMessenger

script_1,script_2,script_3,script_4,script_5 = '','','','',''

def handle_button(x):
    if x == 1:
        subprocess.call([script_1])
    elif x == 2:
        subprocess.call([script_2])
    elif x == 3:
        subprocess.call([script_3])
    elif x == 4:
        subprocess.call([script_4])
    elif x == 5:
        subprocess.call([script_5])
    else:
        print("error, no script found")

def main(argv=None):

    # List of command names (and formats for their associated arguments). These must
    # be in the same order as in the sketch.
    commands = [["server_sec_key","s"],
                ["arduino_sec_key","i"],
                ["button_pressed","i"],
                ["error","s"],
                ["error_ack","s"]]

    if argv == None:
        argv = sys.argv[1:]

    try:
        config_file = argv[0]
    except IndexError:
        err = "Incorrect arguments. Usage:\n\n{}\n\n".format(__usage__)
        raise IndexError(err)

    config = configparser.ConfigParser()
    config.read(config_file)

    serial_device = config['ARDUINO']['serial_device']
    serial_rate = config['ARDUINO']['baud_rate']
    global script_1, script_2, script_3, script_4, script_5
    script_1 = config['SCRIPTS']['script_1']
    script_2 = config['SCRIPTS']['script_2']
    script_3 = config['SCRIPTS']['script_3']
    script_4 = config['SCRIPTS']['script_4']
    script_5 = config['SCRIPTS']['script_5']

    serverkey = config['ARDUINO']['shared_key']

    a = PyCmdMessenger.ArduinoBoard(serial_device,serial_rate)
    c = PyCmdMessenger.CmdMessenger(a,commands)

    c.send("server_sec_key",serverkey)
    received_cmd = c.receive()
    cmd = received_cmd[0]
    received = received_cmd[1]
    print("sent {}, received cmd {} with value {}".format("server_sec_key",cmd,received))

    while True:
        received_cmd = c.receive()
        if received_cmd is not None:
            cmd = received_cmd[0]
            data = received_cmd[1]
            if cmd == "button_pressed":
                handle_button(data[0])


if __name__ == "__main__":
    main()
